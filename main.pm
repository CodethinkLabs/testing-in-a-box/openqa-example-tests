use strict;
use testapi;
use autotest;
use needle;

my $distri = testapi::get_var('CASEDIR') . '/lib/agldistro.pm';
require $distri;
testapi::set_distribution(agldistro->new());

autotest::loadtest "tests/boot.pm";
autotest::loadtest "tests/can_test.pm";
autotest::loadtest "tests/dashboard.pm";
autotest::loadtest "tests/telephony.pm";
autotest::loadtest "tests/topmost_navigation.pm";
autotest::loadtest "tests/bluetooth_connect.pm";
autotest::loadtest "tests/bluetooth_audio.pm";
autotest::loadtest "tests/audio.pm";


1;
