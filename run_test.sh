#!/usr/bin/env bash

cp -r misc/* /usr/lib/os-autoinst/
ADDR="http://172.16.100.21:8080"
SCREEN_ID="$(curl $ADDR/screen/ 2>&1 | grep mode_valid=1 | grep -oP 'ID=\K\d+')"

isotovideo EXTERNAL_VIDEO_ENCODER_CMD="ffmpeg -y -hide_banner -nostats -r 24 -f image2pipe -vcodec ppm -i - -pix_fmt yuv420p -c:v \
  libvpx-vp9 -crf 35 -b:v 1500k -cpu-used 0" EXTERNAL_VIDEO_ENCODER_OUTPUT_FILE_EXTENSION=webm -d --color=yes CASEDIR=. BACKEND=qad \
  QAD_SCREEN_NO=$SCREEN_ID QAD_SERVER_ADDRESS=$ADDR QAD_TOUCH_DEVICE_NO=2 QAD_X_SCALE=17 QAD_Y_SCALE=30 XRES=1920 YRES=1080
if [ "$?" -eq 0 ]; then
  # clean up
  rm -rf autoinst-status.json base_state.json command-server-tmp/ os-autoinst.pid qemuscreenshot/ serial0 testresults/ vars.json \
      video.ogv video.webm video_time.vtt f.txt backend.run
fi
