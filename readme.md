# Tests

Before running, make sure you are connected to the runner via following the documentation [here](https://gitlab.com/CodethinkLabs/testing-in-a-box/testing-in-a-box). Ensure that the system-under-test has a CAN set up, which can be followed [here](https://agl-gsod-2020-demo-mkdocs.readthedocs.io/en/latest/master/apis_services/reference/signaling/5-Usage/).


## Run locally



Get OpenQA worker container:

```bash
docker pull registry.gitlab.com/codethinklabs/testing-in-a-box/openqa-container-worker:main
```

Run openqa

```bash
docker run \
      -it \
      --privileged \
      --network=host
      --device=/dev/kvm \
      -v "`pwd`:/src/" \
      --workdir /src/ \
      -p 6666:6666 \
      --restart=unless-stopped \
      --entrypoint "" \
      registry.gitlab.com/codethinklabs/testing-in-a-box/openqa-container-worker:main \
      bash
```


```bash
cp -r misc/* /usr/lib/os-autoinst/
SCREEN_ID="$(curl http://10.42.1.233:8080/screen/ 2>&1 | grep mode_valid=1 | grep -oP 'ID=\K\d+')"
isotovideo CASEDIR=. BACKEND=qad QAD_SCREEN_NO=$SCREEN_ID QAD_SERVER_ADDRESS=http://10.42.1.233:8080 QAD_TOUCH_DEVICE_NO=2 QAD_X_SCALE=17 QAD_Y_SCALE=30 XRES=1920 YRES=1080
```
