#!/usr/bin/env bash

docker run -it --privileged --device=/dev/kvm -v "`pwd`:/src/" --workdir /src/ -p 6666:6666 --restart=unless-stopped --entrypoint "" gitlab.codethink.co.uk:5000/infrastructure/openqa-container-worker:joshua-update-container bash

string="$(docker container ls | tail -n +2)"
id="$(echo "$string" | awk '{print $1}')"
if [ -n "$id" ]; then
  echo "ID is $id"
  docker container stop $id
  docker container rm $id
fi
