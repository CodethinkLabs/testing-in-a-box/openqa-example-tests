# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;

sub test_dashboard {
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'dashboard');
    assert_and_click('agl-dashboard-page-en', timeout=>3, point_id=>'french');
    assert_and_click('agl-dashboard-page-fr', timeout=>3);
    assert_and_click('agl-dashboard-page-jp', timeout=>3);
    assert_and_click('agl-dashboard-page-ch', timeout=>3);

    # assert that it is the korean page, then change back to English page
    assert_and_click('agl-dashboard-page-ko', timeout=>3);
    assert_and_click('agl-dashboard-page-en', timeout=>3, point_id=>'launcher');
}

sub test_messaging {
    assert_and_click('agl-launcher-screen', timeout=>5, point_id=>'messaging');
    assert_and_click('agl-msg-default-screen', timeout=>3, point_id=>'compose');
    assert_and_click('agl-msg-compose-screen', timeout=>3);
    assert_and_click('agl-msg-default-screen', timeout=>3, point_id=>'launcher');
}


sub run {
    my $self = shift;

    # navigate to the Launcher page.
    touch(106, 970, 0);

    # navigate to the "Messaging" page
    test_messaging();

    test_dashboard();

    assert_screen('agl-launcher-screen', 3);
}


sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
