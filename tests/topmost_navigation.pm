# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;

    # navigate to the Launcher page.
    touch(106, 970, 0);

    # confirm it's indeed on the Launcher page, then navigate to the specified page
    # denoted by the click_point, the media page
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'media-player');

    assert_and_click('agl-media-player-screen', timeout=>3);

    # navigate to the HVAC page
    assert_and_click('agl-hvac-temp-screen', timeout=>3);

    assert_screen('agl-launcher-screen', 3);
}


sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
