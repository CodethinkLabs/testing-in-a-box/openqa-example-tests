# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;

    # put simphoney in mode to listen for connection
    qx( /src/simphoney/tools/controller/controller.py -u localhost:5000 listen );
    # navigate to the Launcher page.
    touch(106, 970, 0);

    # assert we're at Launcher page, navigate to Settings page
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'settings');
    # assert we're at Settings page, navigate to Bluetooth page
    assert_and_click('agl-settings-screen-bluetooth', timeout=>3);
    # assert we're at Bluetooth page, and connect to device
    assert_and_click('agl-bluetooth-connect', timeout=>10, point_id=>'connect');
    # Disconnect from the device
    assert_and_click('agl-bluetooth-disconnect', timeout=>10);
    # remove the device
    touch(589, 175, 0);
    # return to Settings page
    touch(381, 122, 0);
    # assert we're at the Settings page, navigate to Launcher page
    assert_and_click('agl-settings-screen', timeout=>3);
    # return to launcher page
    assert_screen('agl-launcher-screen', 3);
}

sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
