# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;

sub run {
    my $audio_script = get_var('AUDIO_SCRIPT');
    my $audio_save_path = get_var('AUDIO_SAVE_PATH');

    my $self = shift;

    select_console 'ssh-serial-console';

    qx{
        arecord -l | tee -a $audio_save_path/audio-runner-selftest.txt
    };
    # navigate to the Launcher page.
    touch(106, 970, 0);

    ## Play speaker-test
    select_console 'ssh-serial-console';
    background_script_run('speaker-test -t wav -c 4 -l 5', timeout => 40);
    select_console 'sut';
    qx{
        bash $audio_script -n speaker_test -t 20 -d $audio_save_path --nb
    };
    sleep(30);

    ## Play test tracks
    select_console 'ssh-serial-console';
    background_script_run('aplay test-tracks/0-continuous-sine-2000-12000-30s.wav', timeout => 40);
    select_console 'sut';
    qx{
        bash $audio_script -n stereo_continuous_sine -t 20 -d $audio_save_path --nb
    };
    sleep(30);

    select_console 'ssh-serial-console';
    background_script_run('aplay test-tracks/1-5000-both.wav', timeout => 40);
    select_console 'sut';
    qx{
        bash $audio_script -n stereo_5000 -t 20 -d $audio_save_path --nb
    };
    sleep(30);

    select_console 'ssh-serial-console';
    background_script_run('aplay test-tracks/1-10000-both.wav', timeout => 40);
    select_console 'sut';
    qx{
        bash $audio_script -n stereo_10000 -t 20 -d $audio_save_path --nb
    };
    sleep(30);

    select_console 'ssh-serial-console';
    background_script_run('aplay test-tracks/2-20-22k-sweep-44.wav', timeout => 40);
    select_console 'sut';
    qx{
        bash $audio_script -n stereo_sweep_44100 -t 20 -d $audio_save_path --nb
    };
    sleep(30);

    select_console 'ssh-serial-console';
    background_script_run('aplay /home/root/test-tracks/2-20-24k-sweep-48.wav', timeout => 40);
    select_console 'sut';
    qx{
        bash $audio_script -n stereo_sweep_48000 -t 20 -d $audio_save_path --nb
    };
    sleep(30);
    select_console 'sut';
}

sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
