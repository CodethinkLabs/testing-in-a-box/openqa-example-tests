# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;


sub backspace_dialpad {
    my ($n, $timeout) = @_;
    for (my $i = 0; $i < $n; $i++) {
        touch(507, 268, $timeout);
    }
}

sub test_phone_screen {
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'phone');
    touch(270, 885, 1);

    # navigate to the dialpad tab, still on the "Phone" page
    assert_and_click('agl-phone-default-screen', timeout=>5);

    assert_screen('agl-phone-dialpad-screen', 3);
    # simulate a couple of keypad presses, this time, 12345890*#
    touch(640, 739, 0); # 1
    touch(633, 533, 0); # 2
    touch(637, 328, 0); # 3
    touch(839, 735, 0); # 4
    touch(839, 540, 0); # 5
    touch(1053, 538, 0); # 8
    touch(1040, 341, 0); # 9
    touch(1237, 536, 0); # 0
    touch(1239, 741, 0); # '*'
    touch(1239, 330, 0); # '#'

    assert_screen('agl-phone-dialpad-digits', 5);
    # clear 5 characters off the digits, waiting 1 second in-between key-presses
    backspace_dialpad(5, 0);
    assert_screen('agl-phone-dialpad-five', 5);

    backspace_dialpad(5, 0); # clear the last characters off the screen
    assert_screen('agl-phone-dialpad-screen', 5);

    #return to the "Contacts" tab on the phone page
    touch(297, 885, 0.5);

    # navigate to the Launcher page.
    touch(106, 970, 0);
}


sub run {
    my $self = shift;

    # navigate to the Launcher page.
    touch(106, 970, 0);

    # navigate to the "Phone" page
    test_phone_screen();

    assert_screen('agl-launcher-screen', 3);
}


sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
