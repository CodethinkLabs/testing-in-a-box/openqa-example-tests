# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;

    # navigate to the Launcher page.
    touch(106, 970, 0);

    # assert we're at Launcher page, navigate to Settings page
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'settings');
    if (check_screen('agl-bluetooth-close', timeout=>3)) {
        assert_and_click('agl-bluetooth-close', timeout=>3);
    }
    assert_and_click('agl-settings-screen', timeout=>3);
    assert_screen('agl-launcher-screen', 3);
}

sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
