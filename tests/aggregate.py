#!/usr/bin/env python

import argparse
import os
from dataclasses import dataclass
from typing import List


main_header : str = """use strict;
use testapi;
use autotest;
use needle;

my $distri = testapi::get_var('CASEDIR') . '/lib/agldistro.pm';
require $distri;
testapi::set_distribution(agldistro->new());

"""

script_header : str = """#!/usr/bin/env bash

cp -r misc/* /usr/lib/os-autoinst/
ADDR="http://172.16.100.21:8080"
SCREEN_ID="$(curl $ADDR/screen/ 2>&1 | grep mode_valid=1 | grep -oP 'ID=\K\d+')"
"""

use_external_codec_str: str = """
isotovideo EXTERNAL_VIDEO_ENCODER_CMD="ffmpeg -y -hide_banner -nostats -r 24 -f image2pipe -vcodec ppm -i - -pix_fmt yuv420p -c:v \\
  libvpx-vp9 -crf 35 -b:v 1500k -cpu-used 0" EXTERNAL_VIDEO_ENCODER_OUTPUT_FILE_EXTENSION=webm -d --color=yes CASEDIR=. BACKEND=qad \\
  QAD_SCREEN_NO=$SCREEN_ID QAD_SERVER_ADDRESS=$ADDR QAD_TOUCH_DEVICE_NO=2 QAD_X_SCALE=17 QAD_Y_SCALE=30 XRES=1920 YRES=1080"""

no_external_codec_str: str = """
isotovideo -d --color=yes CASEDIR=. BACKEND=qad QAD_SCREEN_NO=$SCREEN_ID QAD_SERVER_ADDRESS=$ADDR QAD_TOUCH_DEVICE_NO=2 QAD_X_SCALE=17 \\
  QAD_Y_SCALE=30 XRES=1920 YRES=1080
"""

script_footer : str = """
if [ "$?" -eq 0 ]; then
  # clean up
  rm -rf autoinst-status.json base_state.json command-server-tmp/ os-autoinst.pid qemuscreenshot/ serial0 testresults/ vars.json \\
      video.ogv video.webm video_time.vtt f.txt backend.run
fi
"""

@dataclass
class AggregateArgs:
    """Argument options for aggregating main.pm from the command line interface"""

    @staticmethod
    def configure_parser(parser: argparse.ArgumentParser) -> None:
        group = parser.add_mutually_exclusive_group()

        parser.add_argument(
            "--use-external-codec",
            action="store_true",
            default=False,
            help="Use ffmpeg instead of the internal codec"
        )

        group.add_argument(
            "--list-names",
            action="store_true",
            help="list all the names available",
        )

        group.add_argument(
            "--generate",
            type=str,
            help="comma separated list of names to generate a main.pm for",
        )


class AggregateCommand:
    def __init__(self, args: AggregateArgs) -> None:
        self.args: AggregateArgs = args
    
    def run(self) -> None:
        if self.args.list_names:
            for name in self.list_all_names():
                print(name)
        elif self.args.generate:
            self.generate_main_for_names()

    def list_all_names(self) -> List[str]:
        from pathlib import Path

        excluded_names: List[str] = ['stub.pm', '.gitignore']

        parent_directory: str = 'tests'
        names: List[str] = []

        for filename in os.listdir(parent_directory):
            if filename in excluded_names or \
                not filename.endswith('.pm') or \
                not os.path.isfile(os.path.join(parent_directory, filename)):
                continue
            names.append(Path(filename).stem.strip())
        return names

    def generate_main_for_names(self) -> None:
        names: List[str] = []
        if self.args.generate.lower() != "all":
            names = list(set([name.strip() for name in self.args.generate.split(',')]) & set(self.list_all_names()))
        else:
            names = self.list_all_names()

        if not names:
            return print('No names found')

        names.sort()
        if os.path.exists('main.pm'):
            counter = 1
            new_filename = f'main_{counter}.pm'
            while os.path.exists(new_filename):
                counter += 1
                new_filename = f'main_{counter}.pm'
            os.rename('main.pm', new_filename)
        
        with open('main.pm', 'w') as f:
            f.write(main_header)
            for name in names:
                f.write(f'autotest::loadtest "tests/{name}.pm";\n')
            f.write('\n\n1;\n')
        print('main.pm generated')

        with open('./run_test.sh', 'w') as f:
            f.write(script_header)
            f.write(use_external_codec_str if self.args.use_external_codec else no_external_codec_str)
            f.write(script_footer)
        print('run_test.sh generated')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="aggregator",
        description="generates the main.pm file needed to run isotovideo",
    )
    AggregateArgs.configure_parser(parser)
    args = parser.parse_args(namespace=AggregateArgs())
    AggregateCommand(args).run()
