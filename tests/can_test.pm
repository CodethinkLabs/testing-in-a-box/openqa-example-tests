use base 'basetest';
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;


    # navigate to the Launcher page.
    touch(106, 970, 0);
    # navigate to HVAC page.
    touch(106, 593, 0);

    # start process of candump and capture output
    my $can_output = system("candump can0 -n 3 &");
    # touch command on hvac fan speed
    touch(424, 870, 0);
    touch(424, 593, 0);
    touch(424, 109, 0);

    # output job success
    if ($can_output == 0) {
      print "CAN Messages received\n";
    }
}

sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
