# vi: set shiftwidth=4 tabstop=4 expandtab:
use base 'basetest';
use strict;
use testapi;
use utils;

sub run {
    my $self = shift;
    my $audio_save_path = get_var('AUDIO_SAVE_PATH');

    # put simphoney in mode to listen for connection
    qx( /src/simphoney/tools/controller/controller.py -u localhost:5000 listen );
    qx( echo 'Begin BT audio playback testing' | tee -a $audio_save_path/pw-playback.txt);
    # navigate to the Launcher page.
    touch(106, 970, 0);

    # assert we're at Launcher page, navigate to Settings page
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'settings');
    # assert we're at Settings page, navigate to Bluetooth page
    assert_and_click('agl-settings-screen-bluetooth', timeout=>3);
    # assert we're at Bluetooth page, and connect to device
    assert_and_click('agl-bluetooth-connect', timeout=>30, point_id=>'media');

    # assert we're at the Media Page
    assert_screen('agl-bluetooth-no-audio', timeout=>10);
    qx( pw-cli s 52 Profile '{ index: 2, save: true}' | tee -a $audio_save_path/pw-playback.txt);
    # qx (pw-play "/src/Plucky Daisy.mp3");
    qx( pw-play test-tracks/0-continuous-sine-2000-12000-30s.wav | tee -a $audio_save_path/pw-playback.txt);
    assert_and_click('agl-bluetooth-no-audio', timeout=>30, point_id=>'launcher');
    assert_and_click('agl-launcher-screen', timeout=>3, point_id=>'settings');

    if (check_screen('agl-settings-screen-bluetooth', timeout=>3)) {
        assert_and_click('agl-settings-screen-bluetooth', timeout=>3);
    }
    # Disconnect from the device
    if (check_screen('agl-bluetooth-disconnect', timeout=>10)) {
        assert_and_click('agl-bluetooth-disconnect', timeout=>10);
    }
    # return to Settings page
    assert_and_click('agl-bluetooth-close', timeout=>3);
    # assert we're at the Settings page, navigate to Launcher page
    assert_and_click('agl-settings-screen', timeout=>3);
    # return to launcher page
    assert_screen('agl-launcher-screen', 3);
}

sub test_flags {
    # Treat success as a milestone worthy of updating ‘last good’ status
    return { fatal => 0, milestone => 1 };
}

1;
