package utils;
use base 'Exporter';
use Exporter;
use testapi;

# export functions
our @EXPORT = qw/touch swipe assert_and_click/;

# Add a touch function for QAD
sub touch {
    my ($self) = @_;
    my $x = shift;
    my $y = shift;
    my $duration = shift;
    my $console = select_console 'sut';
    $console->touch(int($x), int($y), $duration);
}

# Add a swipe function for QAD
sub swipe {
    my ($self) = @_;
    my $x = shift;
    my $y = shift;
    my $x2 = shift;
    my $y2 = shift;
    my $direction = shift;
    my $velocity = shift;
    my $console = select_console 'sut';
    $console->swipe($x, $y, $x2, $y2, $direction, $velocity);
}

# overload the default assert_and_click to map to touch if we are using QAD,
# else use the testapi::assert_and_click
sub assert_and_click {
    if (get_var('BACKEND', 'qad')) {
        my $mustmatch = shift;
        my %args = @_;
        $last_matched_needle = assert_screen($mustmatch, $args{timeout});
        my $relevant_area;
        my $relative_click_point;
        for my $area (reverse @{$last_matched_needle->{area}}) {
            next unless ($relative_click_point = $area->{click_point});
            next if defined $args{point_id} && $relative_click_point->{id} ne $args{point_id};
            $relevant_area = $area;
            last;
        }
        # Calculate the absolute click point.
        my ($x, $y) = testapi::_calculate_clickpoint($last_matched_needle, $relevant_area, $relative_click_point);
        bmwqemu::diag("clicking at $x/$y");
        touch(int($x), int($y), 0);
    } else {
        testapi::assert_and_click(@_);
    }
}

1;
